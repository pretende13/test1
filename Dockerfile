FROM ubuntu:18.04

RUN apt-get update && \
    apt-get install -y nginx && \
    apt-get clean

ARG DEBIAN_FRONTEND=noninteractive
WORKDIR /var/www/html
COPY . /var/www/html
COPY ./default.conf /etc/nginx/sites-available/default
RUN rm /var/www/html/default.conf

RUN addgroup --gid 1000 testing && \
    adduser --uid 1000 --ingroup testing --home /home/testing --shell /bin/sh --disabled-password --gecos "" testing

RUN usermod -aG www-data testing
RUN usermod -aG testing www-data 
RUN chown -R www-data:www-data /var/www/html

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]
